// @flow

import rc from 'rc';
import type {EventlogConfig} from './ConfigType';

// add your own config in .socrates-serverrc file and don't add it to git if you want to keep your secrets.

const config: EventlogConfig = {
  app: {
    name:'socrates-eventlog',
    logLevel: 'trace'
  },
  environment: 'dev',
  jwtSecret: '$lsRTf!gksTRcDWs',
  database: {
    host: '',
    port: 0,
    name: '',
    user: '',
    password: ',',
    debug: false
  },
  server: {
    port: 4444
  },
  mail: {
    user: '',
    password: '',
    host: ''
  },
  kafka: {
    host: 'zookeeper',
    port: 2181,
    topics: ['socrates-events']
  }
};
export default rc(config.app.name, config);