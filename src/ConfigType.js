// @flow

import type {Config} from 'server-commons';

export type EventlogConfig = Config & {
  app: {
    name: string,
    logLevel: string
  },
};

