// @flow

import bunyan from 'bunyan';
import {EventStream, EventTracingStream} from 'socrates-event-stream';
import config from './config';

const logger = bunyan.createLogger({
  name: config.app.name,
  level: config.app.logLevel
});

logger.info('Starting: ', config.app.name);

const topics = config.kafka.topics.map(topic => ({topic}));
const read = new EventStream(topics, config.kafka.host, config.kafka.port, config.app.name, logger);
read.on('error', err => console.error(err));
read.startConsuming();

read.pipe(new EventTracingStream(logger));