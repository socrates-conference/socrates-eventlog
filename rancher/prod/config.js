// @flow
/* eslint-disable */
import type {EventlogConfig} from '../../src/ConfigType';

const config: EventlogConfig = {
  app: {
    name:'socrates-eventlog',
    logLevel: 'trace'
  },
  environment: 'prod',
  database: {
    host: 'socrates-db',
    port: 3306,
    name: 'socrates_db',
    user: '%username%',
    password: '%password%',
    debug: false
  },
  server: {
    port: 4444
  },
  kafka: {
    host: 'zookeeper',
    port: 2181,
    topics: ['socrates-events']
  }
};
export default config;

